import os
contents = [
    {
        "content":"""---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
---        
        """,
        "subdir": "archtypes/",
        "name":"default.md"
    },
    {
        "content":"""{{ define "main" }}
{{ end }}
        """,
        "subdir":"layouts/",
        "name":"index.html"
    },
    {
        "content":"""<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Default Hugo</title>
<body>
    {{- partial "header.html" . -}}
    <div>
        {{- block "main" . }}{{- end }}
    </div>
</body>

</html>
        """,
        "subdir":"layouts/_default/",
        "name":"baseof.html"
    },
    {
        "content":"""{{ define "main" }}
<h1>{{ .Title }}</h1>
{{ range .Pages.ByPublishDate.Reverse }}
<p>
<h3><a href="{{ .RelPermalink }}">{{ .Title }}</a></h3>
{{ partial "metadata.html" . }}
</p>
{{ end }}
{{ end }}
        """,
        "subdir":"layouts/_default/",
        "name":"list.html"
    },
    {
        "content":"""{{ define "main" }}
<div>
    <h1>{{ .Title }}</h1>
    {{ partial "metadata.html" . }}
    <br><br>
    {{ .Content }}
</div>
{{ end }}       
        """,
        "subdir":"layouts/_default/",
        "name":"single.html"
    },
    {
        "content":"""<header>
    {{ range .Site.Menus.main }}
    <a href="{{ .URL }}">
        {{ $text := print .Name | safeHTML }}
        {{ $text }}
    </a>
    {{ end }}
</header>        
        """,
        "subdir":"layouts/partials/",
        "name":"header.html"
    },
    {
        "content":"""{{ $dateTime := .PublishDate.Format "2006-01-02" }}
{{ $dateFormat := .Site.Params.dateFormat | default "Jan 2, 2006" }}
<i data-feather="calendar"></i>
<time datetime="{{ $dateTime }}">{{ .PublishDate.Format $dateFormat }}</time>
{{ with .Params.tags }}
<i data-feather="tag"></i>
{{ range . }}
{{ $href := print (absURL "tags/") (urlize .) }}
<a  href="{{ $href }}">{{ . }}</a>
{{ end }}
{{ end }}      
        """,
        "subdir":"layouts/partials/",
        "name":"metadata.html"
    },
        {
        "content":"""baseURL = "http://example.org/"
languageCode = "en-us"
title = "My New Hugo Site"
sectionPagesMenu = "main"
        """,
        "subdir":"",
        "name":"config.toml"
    },
]
folders = [
    "layouts/",
    "archtypes/",
    "layouts/partials/",
    "layouts/_default/",
    "content"
]
for i in folders:
    os.mkdir(i)
for i in contents:
    f = open(os.path.join(i["subdir"],i["name"]),"w")
    f.write(i["content"])
    f.close()